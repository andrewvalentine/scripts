#!/bin/bash
#######################################################
#declare -x BUILD=2011022409
export PATH="/usr/bin:/bin:/usr/sbin:/sbin"
declare -x MYNAME="configureCUPSKerb.sh"
 
## Executable vars
declare -x awk="/usr/bin/awk"
declare -x grep="/usr/bin/grep"
declare -x logger="/usr/bin/logger"
declare -x lpadmin="/usr/sbin/lpadmin"
declare -x lpstat="/usr/bin/lpstat"
declare -x mkdir="/bin/mkdir"
declare -x perl="/usr/bin/perl"
 
## Get a list of our SMB printers
 
declare -x SMBPRINTERS="$("$lpstat" -v | "$grep" smb | "$perl" -p -e 's/device for (.*): smb.*/$1/g')"
OLDIFS="$IFS"
IFS=$'\n'
for SMBPRINTER in $SMBPRINTERS; do
	"$lpadmin" -p "$SMBPRINTER" -o auth-info-required=negotiate 
done
IFS="$OLDIFS"
