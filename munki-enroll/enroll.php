<?php

require_once( 'cfpropertylist-1.1.2/CFPropertyList.php' );

// Default catalog
$catalog = 'standard';

// Get the varibles passed by the enroll script
$identifier1 = $_POST["identifier1"];
$identifier2 = $_POST["identifier2"];
$hostname   = $_POST["hostname"];

// Split the manifest path up to determine directory structure
//$directories		= explode( "/", $identifier, -1 ); 
//$total				= count( $directories );
//$n					= 0;
//$identifier_path	= "";
//while ( $n < $total )
//	{
//        $identifier_path .= $directories[$n] . '/';
//        $n++;
//	}

// Ensure we aren't nesting a manifest within itself
// Note that this will create a default manifest - it will not honour any options from DS
if ( $identifier == "client-" . $hostname )
	{
		$identifier = "_cg_ru";
	}

// Check if manifest already exists for this machine
echo "\n\tMUNKI-ENROLLER. Checking for existing manifests.\n\n";

if ( file_exists( '../manifests/client-' . $hostname ) )
    {
        echo "\tComputer manifest client-" . $hostname . " already exists.\n";
        echo "\tThis will be replaced.\n\n";
    }
else
    {
        echo "\tComputer manifest does not exist. Will create.\n\n";
    }
	$plist = new CFPropertyList();
	$plist->add( $dict = new CFDictionary() );
        
    // Add manifest to production catalog by default
    $dict->add( 'catalogs', $array = new CFArray() );
    $array->add( new CFString( $catalog ) );
        
    // Add parent manifest to included_manifests to achieve waterfall effect
    $dict->add( 'included_manifests', $array = new CFArray() );
    $array->add( new CFString( $identifier1 ) );

    $dict->add( 'included_manifests', $array = new CFArray() );
    $array->add( new CFString( $identifier2 ) );

        
    // Save the newly created plist
    $plist->saveXML( '../manifests/client-' . $hostname );
    chmod( '../manifests/client-' . $hostname, 0775 );
    echo "\tNew manifest created: client-" . $hostname . "\n";
    echo "\tIncluded Manifest(s): " . $identifier1 . " " . $identifier2 . "\n";
        


?>