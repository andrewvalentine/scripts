Munki-enroll
============

Munki-enroll figures out your computer's hostname, creates a manifest in your munki-repo 
that is unique to your computer, and sets the ClientIdentifier in 
/Library/Preferences/ManagedInstalls.plist to the correct value.

It also adds an included manifest to the computer's manifest, and a catalog.

This is achieved using a script run on the client, which calls a PHP file on the Munki
server. The PHP file is what manipulates the file system on the server. Therefore, the 
following are requirements for this to work:

1.  PHP must be enabled on your munki web server.
2.  Both munkitools (e.g. via MunkiAdmin) and the PHP file need write access to the 
manifests directory. To achieve this, you can either:
    a. give global write access to the manifests directory (bad), or 
    b. add your local user account (the account that's running munkitools) to 
       the _www group. This is still quite bad. 
To achieve the latter, run the following command:
	
	sudo dseditgroup -o edit -a it-zone-d -t user _www
   
munki_enroll.sh is a script that can be run on any Mac with Munki installed. You could 
add the script to your DeployStudio Scripts folder so you can add it to the end of a 
DS workflow.