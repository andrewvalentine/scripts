#!/bin/sh
######################################################################
# syncfiles.sh
# Replicate file trees to ssh server using rsync
# Usage:
#     sh syncfiles.sh
# or call from cron (make sure ssh key is loaded beforehand)
######################################################################
# CHANGE THESE VALUES ONLY #
ServerUser=glgrp-l              ## YOUR USERNAME ON THE SERVER (NORMALLY SAME AS YOUR LOCAL USER)
Server=chm-fs.chm.bris.ac.uk    ## CHANGE TO THE FULL SERVER NAME e.g. chm-fs.chm.bris.ac.uk
LocalFolder=Documents           ## LEAVE AS A DOT TO BACKUP ENTIRE HOME FOLDER, OR CHANGE TO FOR EXAMPLE Documents OR Downloads
                                ## (Note this can only be one folder in this script)
Backup=backup                   ## DESTINATION FOLDER ON SERVER
#####################################################################

# DO NOT CHANGE ANYTHING BELOW HERE #
User=$(whoami)
ssh $ServerUser@$Server "mkdir -p $Backup"
cd /Users/$User
rsync -e ssh -av --delete --delete-excluded --exclude '.*' $LocalFolder $ServerUser@$Server:~/$Backup/
