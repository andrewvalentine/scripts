## SCRIPT TO BACKUP FROM USER Documents FOLDER TO USER'S MyFiles ##
###################################################################
## Variables to be set by user:
$username = glgrp            ## UOB username: e.g. ab12345                                    
$remote_folder = Mac-Backup  ## remote backup directory


## check whether myfiles is already previously mounted by this script
if mount | grep /Volumes/myfiles > /dev/null; then
echo "MyFiles mounted"
else
echo "Mounting MyFiles";
mkdir /Volumes/myfiles;
mount -t smbfs //$username@ads.bris.ac.uk/filestore/myfiles/staff/$username /Volumes/myfiles
fi
cd
echo ""
echo "rsync will copy all updated files from Documents to myfiles/Documents/Mac"
echo ""
echo "Backup started "
date
rsync -avl --delete --stats --progress --exclude '.DS_Store' \
./Documents /Volumes/myfiles/Documents/Mac
echo "Backup finished "
date
