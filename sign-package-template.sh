#!/bin/bash

# Sign the pkg
productsign --sign "Graham Pugh" Print-Release-Staff-Unsigned.pkg Print-Release-Staff.pkg

# Remove previous instance of dmg


# Create new dmg
mkdir tmp
cp Print-Release-Staff.pkg tmp/
hdiutil create \
	-volname "Print-Release-Staff" \
	-srcfolder ./tmp \
	-ov \
	Print-Release-Staff.dmg
rm -rf tmp

exit 0
