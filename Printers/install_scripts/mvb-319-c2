#!/bin/bash

### Variables. Edit these.
printername="MVB3.19-C2"
location="MVB 3rd floor corridor"
gui_display_name="Canon Photocopier IR6870"
address="smb://its-zoneeprint.cse.bris.ac.uk/MVB3.19-C2"

# leave this empty if you wish to use the generic PS PPD print driver
# or make sure the file exists in the ../drivers directory
# and has been explicitly set in the Makefile
driver_file="CNMCIRAC5235S2.ppd.gz"

# set this to 1 if you wish to use the generic PS print driver
generic_ppd=0

# set this to 1 if you wish this printer to be default (note the last script run will take precedence).
default_printer=0

# Populate these options if you want to set specific options for the printer. E.g. duplexing installed, etc.
option_1="auth-info-required=username,password"
option_2="APOptionalDuplexer=True"
option_3="Duplex=DuplexNoTumble"
option_4="PageSize=A4"

### Stop editing.


# driver locations
driver_src="/Library/Management/Printers/Drivers/${driver_file}"

if [ $generic_ppd = 1 ]; then 
driver_ppd="/System/Library/Frameworks/ApplicationServices.framework/Versions/A/Frameworks/PrintCore.framework/Versions/A/Resources/Generic.ppd"
else
	driver_ppd_dest="/Library/Printers/PPDs/Contents/Resources"
	driver_ppd="${driver_ppd_dest}/${driver_file}"
fi

# Delete any existing instance of this printer
/usr/sbin/lpadmin -x "$printername"

# Now we can install the printer.
/usr/sbin/lpadmin \
        -p "$printername" \
        -L "$location" \
        -D "$gui_display_name" \
        -v "${address}" \
        -P "$driver_ppd" \
        -o "$option_1" \
        -o "$option_2" \
        -o "$option_3" \
        -o "$option_4" \
        -o printer-is-shared=false \
        -o printer-error-policy=abort-job \
        -E

# Make this printer default.
if [ $default_printer = 1 ]; then
	/usr/sbin/lpadmin -d "$printername"
fi

# Enable and start the printers on the system (after adding the printer initially it is paused).
/usr/sbin/cupsenable $(lpstat -p | grep -w "printer" | awk '{print$2}')
