#!/bin/bash

# Run this script after you have used "make pkg" to create the unsigned package
# You need to have a valid Apple Developer ID in your Keychain for this to work

# In your Makefile, you should name your package as PACKAGE_NAME-unsigned.pkg 
# where PACKAGE_NAME matches the entry below:
PACKAGE_NAME="Student-Print-Release"

# Enter your Apple Developer ID common name
COMMON_NAME="Graham Pugh"

### Don't edit beyond here

# Some defs
Pkg_src="${PACKAGE_NAME}-unsigned.pkg"
Pkg_signed_name="${PACKAGE_NAME}.pkg"
DMG_name="${PACKAGE_NAME}.dmg"

# Sign the pkg
productsign --sign "${COMMON_NAME}" "${Pkg_src}" "${Pkg_signed_name}"

# Create new dmg
mkdir tmp
cp ${Pkg_signed_name} tmp/
hdiutil create \
	-volname "${PACKAGE_NAME}" \
	-srcfolder ./tmp \
	-ov \
	${DMG_name}
rm -rf tmp

exit 0
