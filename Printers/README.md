Printer Install Package Maker
=============================

The Luggage is a pre-requisite for this system:
(https://github.com/unixorn/luggage)

The 'install_scripts' folder is your catalogue of install scripts.
Copy the file template.sh to a new file and edit it to add another printer to the catalogue.

The 'drivers' folder is your repository for drivers required in your install scripts.
These should be .ppd.gz files.

The 'src' folder is your repository of source files from which to make your packages
using The Luggage.

*  Copy the 'template' folder to a new folder.
*  Edit the Makefile to point to the install scripts and drivers you want in your package.
*  Run: `make pkg`
*  If you used the signed option, you can then run `./dmg-it.sh` to create a DMG (you can run it anyway, but the DMG won't be signed).


