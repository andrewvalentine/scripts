#!/bin/bash

# Run this script after "make pkg" to create a DMG
# if you have already signed your pkg in the Makefile
# or if you dont want to sign it.
#
# This version of the script will create a DMG for each pkg in the folder it is in.

APP_TO_DMG="EndNote X7"

output_Name="$APP_TO_DMG.dmg"
echo "APP->DMG maker."
hdiutil create \
	-size 400m \
	-nospotlight \
	-fs HFS+ \
	-volname "$APP_TO_DMG" \
	-ov \
	"$output_Name"
hdiutil mount "$output_Name"
cp -r "/Applications/$APP_TO_DMG" "/Volumes/$APP_TO_DMG/"
hdiutil unmount "/Volumes/$APP_TO_DMG/"
exit 0
