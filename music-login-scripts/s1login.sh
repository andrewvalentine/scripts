#!/bin/bash

# Remove old folders for share connection
if [ -d "/volumes/user_folder" ] ; then
   rm -d /volumes/user_folder
fi
if [ -d "/volumes/central_store" ] ; then
   rm -d /volumes/central_store
fi
if [ -d "/volumes/staff_shared" ] ; then
   rm -d /volumes/staff_shared
fi
if [ -d "/volumes/music_distribution" ] ; then
   rm -d /volumes/music_distribution
fi
if [ -d "/volumes/music_software" ] ; then
   rm -d /volumes/music_software
fi
if [ -d "/volumes/DO_NOT_TOUCH" ] ; then
   rm -d /volumes/DO_NOT_TOUCH
fi

# Clear the 'workspace' local drive
rm -dfr /volumes/workspace/*

# Fail-safe clear-up to stop user home folders accumulating
# Shouldn't be necessary, but clearance sometimes fails in s1logout.sh
filelist=`ls /users`
for filetotrash in $filelist ; do
    if [ ! $filetotrash = "it-zone-d" ] ; then    # added for testing by GP
    if [ ! $filetotrash = "musiadmin" ] ; then
        if [ ! $filetotrash = "homefolderuser" ] ; then
            if [ ! $filetotrash = "$1" ] ; then
                if [ ! $filetotrash = "Shared" ] ; then
		    if [ ! $filetotrash = ".localized" ] ; then
                        rm -dfrv /users/$filetotrash
		    fi
                fi
            fi
        fi
    fi
    fi
done

# Empty any printer queues
cancel -a -

# Delete all existing printers
#lpstat -p | awk '{print $2}' | while read printer
#do
#lpadmin -x $printer
#done

lpadmin -R "Photocopier unrestricted"
lpadmin -R "S1 Printer unrestricted"

# Set up a variable to define the location of the file that will contain the user id information
idfileloc=/tmp/idfile

# Write full group/UOB details of the logged in user to file 'idfile'
id $1 > $idfileloc

# Test for local admin
if grep -iq musiadmin $idfileloc ; then
    user_group=localadmin
fi

# Test for admin
if grep -iq MUSI_admin $idfileloc ; then
    user_group=admin
fi

# Test for member of staff
if grep -iq MUSI_staff $idfileloc ; then
    user_group=staff
fi

# Test for taught postgraduate
if grep -iq MUSI_tgrads $idfileloc ; then
    user_group=pg
fi

# Test for research postgraduate
if grep -iq MUSI_pgrads $idfileloc ; then
    user_group=pg
fi

# Test for undergraduate
if grep -iq MUSI_ugrads $idfileloc ; then
    user_group=ug
fi

# Clean up after ourselves
rm $idfileloc

# Connect to printers based on type of user

# Local admin
if [ "$user_group" = "localadmin" ] ; then
    lpadmin -p "Photocopier_unrestricted" -v "lpd://137.222.26.201/" -D "Photocopier unrestricted" -L "Outside F12, Victoria Rooms" -E -P "/library/printers/ppds/contents/resources/Canon_iR3225_general.ppd.gz" -o printer-is-shared=false
    lpadmin -p "S1_Printer_unrestricted" -v "lpd://137.222.26.170/" -D "S1 Printer unrestricted" -L "Room S1, Victoria Rooms" -E -P "/library/printers/ppds/contents/resources/HP LaserJet P3010 Series.gz" -o printer-is-shared=false
    lpadmin -d "S1_Printer_unrestricted"
    exit
fi

# Admin
if [ "$user_group" = "admin" ] ; then
    lpadmin -p "Photocopier_unrestricted" -v "lpd://137.222.26.201/" -D "Photocopier unrestricted" -L "Outside F12, Victoria Rooms" -E -P "/library/printers/ppds/contents/resources/Canon_iR3225_general.ppd.gz" -o printer-is-shared=false
    lpadmin -p "S1_Printer_unrestricted" -v "lpd://137.222.26.170/" -D "S1 Printer unrestricted" -L "Room S1, Victoria Rooms" -E -P "/library/printers/ppds/contents/resources/HP LaserJet P3010 Series.gz" -o printer-is-shared=false
    lpadmin -d "S1_Printer_unrestricted"
    exit
fi

# Staff
if [ "$user_group" = "staff" ] ; then
    lpadmin -p "Photocopier_unrestricted" -v "lpd://137.222.26.201/" -D "Photocopier unrestricted" -L "Outside F12, Victoria Rooms" -E -P "/library/printers/ppds/contents/resources/Canon_iR3225_general.ppd.gz" -o printer-is-shared=false
    lpadmin -p "S1_Printer_unrestricted" -v "lpd://137.222.26.170/" -D "S1 Printer unrestricted" -L "Room S1, Victoria Rooms" -E -P "/library/printers/ppds/contents/resources/HP LaserJet P3010 Series.gz" -o printer-is-shared=false
    lpadmin -d "S1_Printer_unrestricted"
    exit
fi

# Undergraduates - commented out as we're going to use Munki for this.
if [ "$user_group" = "ug" ] ; then
    lpadmin -p "Photocopier_A4_via_PAS" -v "smb://is-print2.pcr.bris.ac.uk/MusicCopierA4" -D "Photocopier A4 via PAS" -L "Outside F12, Victoria Rooms" -E -P "/library/printers/ppds/contents/resources/Canon_iR3225_A4.ppd.gz" -o printer-is-shared=false -o auth-info-required=negotiate
    lpadmin -p "Photocopier_A3_via_PAS" -v "smb://is-print2.pcr.bris.ac.uk/MusicCopierA3" -D "Photocopier A3 via PAS" -L "Outside F12, Victoria Rooms" -E -P "/library/printers/ppds/contents/resources/Canon_iR3225_A3.ppd.gz" -o printer-is-shared=false -o auth-info-required=negotiate
    lpadmin -p "S1_Printer_via_PAS" -v "smb://is-print2.pcr.bris.ac.uk/MusicS1" -D "S1 Printer via PAS" -L "Room S1, Victoria Rooms" -E -P "/library/printers/ppds/contents/resources/HP LaserJet P3010 Series.gz" -o printer-is-shared=false -o auth-info-required=negotiate
    lpadmin -d "S1_Printer_via_PAS"
    exit
fi

# Postgraduates
if [ "$user_group" = "pg" ] ; then
    lpadmin -p "Photocopier_unrestricted" -v "lpd://137.222.26.201/" -D "Photocopier unrestricted" -L "Outside F12, Victoria Rooms" -E -P "/library/printers/ppds/contents/resources/Canon_iR3225_general.ppd.gz" -o printer-is-shared=false
    lpadmin -p "S1_Printer_unrestricted" -v "lpd://137.222.26.170/" -D "S1 Printer unrestricted" -L "Room S1, Victoria Rooms" -E -P "/library/printers/ppds/contents/resources/HP LaserJet P3010 Series.gz" -o printer-is-shared=false
    lpadmin -d "S1_Printer_unrestricted"
    exit
fi

# Set up fresh iTunes folder
sudo rm -dfr /users/shared/itunes
unzip -o -qq /users/shared/itunes.zip -d /users/shared
sudo rm -dfr /users/shared/__MACOSX

exit 0
