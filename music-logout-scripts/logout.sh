#!/bin/sh
onLogout() {
    echo 'Clearing Kerberos for logout' >> ~/Library/Logs/logout.sh.log
    kdestroy -a
    exit
}

trap 'onLogout' SIGINT SIGHUP SIGTERM
while true; do
    sleep 86400 &
    wait $!
done