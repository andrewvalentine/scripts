#!/bin/bash

### A script to search your Munki repo and offer to delete items. Use with care! 
### It will search all directories including pkgs, pkgsinfo and icons.
###    Syntax: ./munkirm.sh -d <search-term>
###    For example, ./munkirm.sh -d Xcode
### Search is case insensitive. 
### Options are y or Y to delete, n, N or anything else to skip, and q or Q to quit

# Munki repo - change to match your path
MUNKI_REPO="/Volumes/munki_repo"

# First level
while getopts ":d:" opt; do
	case $opt in
    d)
    	# echo "-d was triggered, Parameter: $OPTARG" >&2
    	PKG="$OPTARG"
    	
    	# write find results to temporary file
    	#find $MUNKI_REPO -type f -iname "*$PKG*" > /tmp/list.txt
    	find $MUNKI_REPO -type f -iname "*$PKG*" | awk -v FS=/ -v OFS=/ '{ print $NF,$0 }' | sort -n -t / | cut -f2- -d/ > /tmp/list.txt
    	;;
    \?)
      	echo "Invalid option: -$OPTARG" >&2
      	exit 1
      	;;
    :)
      	echo "Option -$OPTARG requires an argument." >&2
      	exit 1
      	;;
  	esac
done

if [ -s /tmp/list.txt ]; then
	# Print the results first
	echo
	echo "Found these files:"
	echo
	cat /tmp/list.txt
	echo
	
	# Now offer up each file for deletion
	for file in `cat /tmp/list.txt`; do
		read -p "Delete $file (y/n/q)?  " -n 1 input    
		case $input in 
			y|Y ) 	echo
					rm -r $file 
					echo "$file Deleted!"
					REMAKE=1
					;;
			q|Q ) echo; break;;
			* ) echo; echo "skipped";;
		esac
	done
else
	# No match, therefore no file
	echo "Files not found!"
fi

rm /tmp/list.txt

echo

# Update the repo if required
if [[ $REMAKE == 1 ]]; then
	if hash makecatalogs 2>/dev/null; then
		echo
		echo "### Catalogs have changed - running makecatalogs"
		echo
		/usr/local/munki/makecatalogs $MUNKI_REPO
	else
		echo
		echo "### Catalogs have changed but you don't have makecatalogs installed!"
		echo "### Now run makecatalogs on a Mac with munkitools installed to effect the changes"
		echo
	fi
fi

exit 0

