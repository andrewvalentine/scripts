#!/bin/sh

# Adapted from script by "henkb"; http://deploystudio.com/Forums/viewtopic.php?id=4914

echo "### Checking Filesystem-architecture on hard disk (HFS or CoreStorage). ###"
echo ""
TYPE=`diskutil list | grep -A4 /dev/disk0 | tail -n 1 | awk '{print $2}' | cut -d "_" -f2`
if [ "$TYPE" = "HFS" ]
    then echo "### Architecture is HFS. Skipping additional steps and continue with regular DeployStudio-workflow. ###"
    sleep 5
    exit 0
fi
sleep 5
if [ "$TYPE" = "CoreStorage" ]
        then FVCHECK=`fdesetup status | grep On`
        if [ "$FVCHECK" = "On" ]
                then echo "### FileVault is enabled! Fusion Drive not possible ###"
        else
                echo "### Architecture is CoreStorage, Fusion Drive is possible. ###"
				echo "RuntimeSelectWorkflow: 2790BE8F-C912-45B9-A820-19A0E6204586"  # Substitute your Fusion workflow identifier
        fi
else
    echo ""
    echo ""
    echo "#########################################################################"
    echo "###           Problem finding filestructure of harddisk! Check it with Disk Utility!          ###"
    echo "#########################################################################"
    sleep 10
fi
exit 0
