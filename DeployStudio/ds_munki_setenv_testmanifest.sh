#!/bin/sh

# Set the URL to the Munki Repository
defaults write /Library/Preferences/ManagedInstalls SoftwareRepoURL "http://deploystudio.isys.bris.ac.uk:8080"

# Set which Munki Manifest to place the client. "Universal" is the generic baseline production manifest.
defaults write /Library/Preferences/ManagedInstalls ClientIdentifier "test_munki_enroll"

# This setting determines whether Munki should handle Apple Software Updates
defaults write /Library/Preferences/ManagedInstalls InstallAppleSoftwareUpdates -bool True

# The existence of this file prods Munki to check for and install updates upon startup
touch /Users/Shared/.com.googlecode.munki.checkandinstallatstartup

exit 0
