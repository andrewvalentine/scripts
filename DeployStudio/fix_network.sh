#!/bin/sh
# Remove Network Preferences plist
# Fixes issue with imaging 2012 MBAirs
rm -rf /Volumes/Macintosh\ HD/Library/Preferences/SystemConfiguration/NetworkInterfaces.plist
rm -rf /Volumes/Macintosh\ HD/Library/Preferences/SystemConfiguration/preferences.plist
exit 0