#!/bin/bash 

# The Munki Repo URL
MUNKI_REPO_URL="http://munki.isys.bris.ac.uk"

# The ClientIdentifier - the default is "_cg_ru"

COMPFIELD1=`defaults read /Library/Preferences/com.apple.RemoteDesktop Text1`
COMPFIELD2=`defaults read /Library/Preferences/com.apple.RemoteDesktop Text2`
if [ $COMPFIELD1 = "ZA" ]; then
	IDENTIFIER1="_cg_za"
elif [ $COMPFIELD1 = "ZB" ]; then
	IDENTIFIER1="_cg_zb"
elif [ $COMPFIELD1 = "ZC" ]; then
	IDENTIFIER1="_cg_zc"
elif [ $COMPFIELD1 = "ZD" ]; then
	IDENTIFIER1="_cg_zd"
elif [ $COMPFIELD1 = "ZE" ]; then
	IDENTIFIER1="_cg_ze"
elif [ $COMPFIELD1 = "ZF" ]; then
	IDENTIFIER1="_cg_zf"
else
	IDENTIFIER1="_cg_ru"
fi

if [ $COMPFIELD2 = "AD" ]; then
	IDENTIFIER2="_cg_ru_ad"
elif  [ $COMPFIELD2 = "ADL" ]; then
	IDENTIFIER2="_cg_ru_eduroam"
elif [ $COMPFIELD2 = "ES" ]; then
	IDENTIFIER2="_cg_zd_oa_earthsci"
fi


echo "Compfield1: $COMPFIELD1"
echo "Identifier(s): $IDENTIFIER1 $IDENTIFIER2"

# This setting determines whether Munki should handle Apple Software Updates
# Set to false if you want Munki to only deal with third party software
defaults write /Library/Preferences/ManagedInstalls InstallAppleSoftwareUpdates -bool True

# The existence of this file prods Munki to check for and install updates upon startup
# If you'd rather your clients waited for an hour or so, comment this out
touch /Users/Shared/.com.googlecode.munki.checkandinstallatstartup

# Leave this unless you have put your munki-enroll script somewhere unusual
SUBMITURL="$MUNKI_REPO_URL/munki-enroll/enroll.php"

# Figures out the computer's local host name - don't use ComputerName as this may contain bad characters
LOCALHOSTNAME=$( scutil --get LocalHostName );

# Checks whether it is a valid IT tag - you can choose your own naming scheme
ITTAGCHECK=`echo $LOCALHOSTNAME | grep -iE '\<IT[0-9]{6}\>'`
if [ $? -ne 0 ]; then
	# Sets the LocalHostName to the serial number if we don't have an IT tag name
	SERIAL=`/usr/sbin/system_profiler SPHardwareDataType | /usr/bin/awk '/Serial\ Number\ \(system\)/ {print $NF}'`
	scutil --set LocalHostName "$SERIAL"
	LOCALHOSTNAME="$SERIAL"
fi

# set the ClientIdentifier to "client-LOCALHOSTNAME
defaults write /Library/Preferences/ManagedInstalls ClientIdentifier "client-$LOCALHOSTNAME"

# Sets the URL to the Munki Repository
defaults write /Library/Preferences/ManagedInstalls SoftwareRepoURL "$MUNKI_REPO_URL"

# Application paths
CURL="/usr/bin/curl"

$CURL --max-time 5 --data \
	"hostname=$LOCALHOSTNAME&identifier1=$IDENTIFIER1&identifier2=$IDENTIFIER2" \
    $SUBMITURL
 	 
exit 0