#!/bin/sh

# Set the URL to the Munki Repository
defaults write /Library/Preferences/ManagedInstalls SoftwareRepoURL "http://deploystudio.isys.bris.ac.uk:8080"

# This setting determines whether Munki should handle Apple Software Updates
defaults write /Library/Preferences/ManagedInstalls InstallAppleSoftwareUpdates -bool True

# The existence of this file prods Munki to check for and install updates upon startup
touch /Users/Shared/.com.googlecode.munki.checkandinstallatstartup

exit 0
