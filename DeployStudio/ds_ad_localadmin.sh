#!/bin/bash

# This script can be used in a DeployStudio workflow to give local administrator rights to an active directory group.

# Edit these two lines
DomainNetBIOSName="uob"
ADGroupToAdd="${DS_HOSTNAME}-BA"

# Don't edit beyond here
CombinedADGroupToAdd="${DomainNetBIOSName}\\${ADGroupToAdd}"

echo "Setting Local Administrators:"
echo "Host Name = ${DS_HOSTNAME}"
echo "Group = ${CombinedADGroupToAdd}"
dsconfigad -groups "${CombinedADGroupToAdd}"
