#!/bin/bash 

IDENTIFIER="site_default"

HOSTNAME=$( scutil --get ComputerName );

SUBMITURL="http://deploystudio.isys.bris.ac.uk:8080/munki-enroll/enroll.php"

# Application paths
CURL="/usr/bin/curl"

$CURL --max-time 5 --data \
	"hostname=$HOSTNAME&identifier=$IDENTIFIER" \
    $SUBMITURL
 	
# IDENTIFIER_PATH=$( echo "$IDENTIFIER" | sed 's/\/[^/]*$//' ); 
 	
defaults write /Library/Preferences/ManagedInstalls ClientIdentifier "client-$HOSTNAME"
 
exit 0