#!/bin/sh

# Adapted from script by "henkb"; http://deploystudio.com/Forums/viewtopic.php?id=4914

echo ""
echo "### Checking filesystem architecture on disk0 (HFS or CoreStorage). ###"
TYPE=`diskutil list | grep -A4 /dev/disk0 | tail -n 1 | awk '{print $2}' | cut -d "_" -f2`
if [ "$TYPE" = "HFS" ]
    then echo "# Architecture is HFS. Skipping additional steps and continue with regular DeployStudio-workflow."
elif [ "$TYPE" = "CoreStorage" ]
	then echo "# disk0 is CoreStorage. Checking for disk1."
	DISK1_IS_SATA=`system_profiler SPSerialATADataType | grep "BSD Name: disk1"`
	if [ "$?" = 0 ] 
		then echo "# disk1 is SATA. Checking file system"
		SECOND_TYPE=`diskutil list | grep -A4 /dev/disk1 | tail -n 1 | awk '{print $2}' | cut -d "_" -f2`
		if [ "$SECOND_TYPE" = "CoreStorage" ]
			then echo "# Two disks with CoreStorage volumes found. Existing Architecture is Fusion Drive."
			echo "RuntimeSelectWorkflow: 2790BE8F-C912-45B9-A820-19A0E6204586"  # Substitute your Fusion workflow identifier
		else
			echo "# disk1 found, but not CoreStorage" 
		fi
	else
		echo "# disk1 not found, or is external." 
	fi
	FVCHECK=`fdesetup status | grep On`
	if [ "$?" = 0 ]
		then echo "# FileVault is enabled."
	fi
else
    echo ""
    echo "### Problem finding filestructure of hard drive(s)! Check it with Disk Utility!"
fi
exit 0
