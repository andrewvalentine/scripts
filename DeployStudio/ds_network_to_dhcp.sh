#!/bin/bash

networksetup -setdhcp "Ethernet" empty
networksetup -setdnsservers "Ethernet" empty
networksetup -setsearchdomains "Ethernet" empty