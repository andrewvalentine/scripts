#!/bin/bash 

# The Munki Repo URL
MUNKI_REPO_URL="http://munki.isys.bris.ac.uk"

# The ClientIdentifier - the default is "_cg_ru"
IDENTIFIER="_cg_ru"

# This setting determines whether Munki should handle Apple Software Updates
# Set to false if you want Munki to only deal with third party software
defaults write /Library/Preferences/ManagedInstalls InstallAppleSoftwareUpdates -bool True

# The existence of this file prods Munki to check for and install updates upon startup
# If you'd rather your clients waited for an hour or so, comment this out
touch /Users/Shared/.com.googlecode.munki.checkandinstallatstartup

# Leave this unless you have put your munki-enroll script somewhere unusual
SUBMITURL="$MUNKI_REPO_URL/munki-enroll/enroll.php"

# Figures out the computer's local host name - don't use ComputerName as this may contain bad characters
LOCALHOSTNAME=$( scutil --get LocalHostName );

# Checks whether it is a valid IT tag - you can choose your own naming scheme
ITTAGCHECK=`echo $LOCALHOSTNAME | grep -iE '\<IT[0-9]{6}\>'`
if [ $? -ne 0 ]; then
	# Sets the LocalHostName to the serial number if we don't have an IT tag name
	SERIAL=`/usr/sbin/system_profiler SPHardwareDataType | /usr/bin/awk '/Serial\ Number\ \(system\)/ {print $NF}'`
	scutil --set LocalHostName "$SERIAL"
	LOCALHOSTNAME="$SERIAL"
fi

# set the ClientIdentifier to "client-LOCALHOSTNAME
defaults write /Library/Preferences/ManagedInstalls ClientIdentifier "client-$LOCALHOSTNAME"

# Sets the URL to the Munki Repository
defaults write /Library/Preferences/ManagedInstalls SoftwareRepoURL "$MUNKI_REPO_URL"

# Application paths
CURL="/usr/bin/curl"

$CURL --max-time 5 --data \
	"hostname=$LOCALHOSTNAME&identifier=$IDENTIFIER" \
    $SUBMITURL
 	 
exit 0