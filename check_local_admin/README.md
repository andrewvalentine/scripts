# check_local_admin
## ACTIVE DIRECTORY USER PERMISSIONS SCRIPT
by Graham Pugh
Props to Jeff Kelley, Graham Gilbert and various others for elements of script

This script looks at the "Allow Administration By" field of the `dsconfigad -show` 
command and checks each of the Active Directory users with mobile accounts on the 
computer to check whether they should have local admin rights.  It amends each user's 
membership of the local 'admin' group accordingly.

This ensures that users retain admin rights when away from the bound network,
but removes any users that are no longer eligible for admin rights once the 
computer sees the network again.

This script can be run as a LaunchDaemon to run when a user logs in, 
to automatically check whether they have admin rights or not.

The Makefile creates a package which, when installed, loads the LaunchDaemon. 
Therefore, the package requests a restart.
