#!/bin/bash

ITEM_NAME="Endnote X7"
PKG_NAME="EndNote-X7-17.1.0.9529.dmg"
PKG_SOURCE="/Users/Shared/munki_repo/pkgs/Thomson_Reuters/$PKG_NAME"
PKG_DEST="/Users/Shared/munki_repo/pkgs/Thomson_Reuters"
ITEM_DEST="/Applications"

#Check there's a directory ready for Endnote
#mkdir -p "$PKG_DEST"

#cp "$PKG_SOURCE" "$PKG_DEST"

makepkginfo "$PKG_DEST/$PKG_NAME" --owner=root --group=wheel --mode=go-w --item="$ITEM_NAME" --destinationpath="$ITEM_DEST"
