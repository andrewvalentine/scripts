#!/bin/bash

# Run this script after "make pkg" to create a DMG
# if you have already signed your pkg in the Makefile
# or if you dont want to sign it.
#
# This version of the script will create a DMG for each pkg in the folder it is in.

APP_TO_DMG="EndNote X7"
echo "APP->DMG maker. Making a DMG from /Applications/$APP_TO_DMG ..."
mkdir -p tmp
cp -r "/Applications/$APP_TO_DMG" "tmp/"
output_Name="$APP_TO_DMG.dmg"
hdiutil create \
	-srcfolder "./tmp" \
	-volname "$APP_TO_DMG" \
	-ov \
	"$output_Name"
rm -rf "tmp"
exit 0
